package com.example.demo.exeption;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class ResourceNotFoundExection extends RuntimeException{
    public ResourceNotFoundExection(String message) {
        super(message);
    }

    public ResourceNotFoundExection (String message, Throwable cause) {
        super(message, cause);
    }
}

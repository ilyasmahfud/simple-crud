package com.example.demo.Model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "Books")
@Getter
@Setter
public class Book {
    @Id //primary key
    @GeneratedValue(strategy = GenerationType.IDENTITY) //teknik mengurutkan id
    @Column(name = "id")// kolom name
    private long id;

    @Column(name = "name")
    private String bookName;
}

package com.example.demo.controller;

import com.example.demo.Model.Book;
import com.example.demo.exeption.ResourceNotFoundExection;
import com.example.demo.repository.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1")
public class BookController {
    @Autowired
    private BookRepository bookRepository;

    // get all
    @GetMapping("/books")
    public ResponseEntity<Object> GetAllBooks(){
        // Object berarti menerima data dlm bentuk apa aja
        List<Book> books = bookRepository.findAll();
        return new ResponseEntity<>(books, HttpStatus.OK);
    }

    // get spesifik
    @GetMapping("/books/{id}")
    public ResponseEntity<Object> GetSingleBook(@PathVariable Long id){
        Book book_detail = bookRepository.findById(id)
                .orElseThrow(()-> new ResourceNotFoundExection(("book not found")));
        return new ResponseEntity<>(book_detail, HttpStatus.OK);
    }

    @PostMapping("/books")
    public ResponseEntity<Object> CreateBooks(@RequestBody Book book){
        return new ResponseEntity<>(bookRepository.save(book), HttpStatus.CREATED);
    }

    @PutMapping("/books/{id}")
    public ResponseEntity<Object> UpdateBooks(@PathVariable Long id,
                                              @RequestBody Book book) {
        Book book_detail = bookRepository.findById(id)
                .orElseThrow(()-> new ResourceNotFoundExection(("book not found")));
        book_detail.setBookName(book.getBookName());
        bookRepository.save(book_detail);
        return new ResponseEntity<>(book_detail, HttpStatus.OK);
    }

    // delete
    @DeleteMapping("/books/{id}")
    public ResponseEntity<Object> DeleteBook(@PathVariable Long id){
        Book book_detail = bookRepository.findById(id)
                .orElseThrow(()-> new ResourceNotFoundExection(("book not found")));
        bookRepository.delete(book_detail);
        return new ResponseEntity<>(book_detail, HttpStatus.OK);
    }
}
